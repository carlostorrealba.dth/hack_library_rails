class Appoinment < ApplicationRecord
  belongs_to :customer
  belongs_to :employee
  has_many :orders
  has_many :invoices, through: :orders
end
