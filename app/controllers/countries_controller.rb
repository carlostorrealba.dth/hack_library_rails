class CountriesController < ApplicationController

  def index
    @countries = Country.all
    render json: @countries
  end

  def show
    @country = Country.find_by(id: params[:id])
    if @country
      render json: @country
    else
      render json: params
    end
  end

  def create
    @country = Country.new(params[:country])
    if @country.save
      render json: @country
    else
      render json: { error: 'could not be created' }
    end
  end

  def update
    @country = Country.find_by(id: params[:id])
    if @country.update(params[:country])
      render json: @country
    else
      render json: { error: 'could not be updated' }
    end
  end

  def destroy
    @country = Country.find_by(id: params[:id])
    if @country.destroy
      render json: @country
    else
      render json: { error: 'could not be destroyed' }
    end
  end
end
