class BooksAssetsController < ApplicationController
  def index
    @book = Book.find_by(params[:id])
    render json: @book.assets
  end
end
