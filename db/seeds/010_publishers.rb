# frozen_string_literal: true

countries = Country.all

20.times do
  Publisher.create do |publisher|
    publisher.name    = Faker::Book.publisher
    publisher.country = countries.sample
  end
end
