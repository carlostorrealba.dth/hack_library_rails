# frozen_string_literal: true

publishers = Publisher.all

100.times do
  Book.create(title: Faker::Book.title,
              author: Faker::Book.author,
              price: Faker::Number.decimal(2),
              publisher_id: publishers.sample.id)
end
