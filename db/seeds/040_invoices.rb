# frozen_string_literal: true

customers = Customer.all
max_index = customers.size - 1
300.times do
  Invoice.create(date: Faker::Date.backward(365),
                 is_paid: Faker::Boolean.boolean,
                 customer: customers[rand(0..max_index)])
end
