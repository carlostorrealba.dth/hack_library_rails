# frozen_string_literal: true

def create_details(invoice, book)
  invoice.details.create do |detail|
    detail.quantity = rand(1..10)
    detail.price    = book.price
    detail.book     = book
  end
end

invoices  = Invoice.all
books     = Book.all

invoices.each do |invoice|
  if rand(1..100).odd?
    2.times do
      create_details(invoice, books.sample)
    end
  else
    create_details(invoice, books.sample)
  end
end
