class CreateDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :details do |t|
      t.integer :quantity
      t.references :book, foreign_key: true
      t.references :invoice, foreign_key: true
      t.decimal :price, precision: 5, scale: 2
    end
  end
end
