class CreateInvoices < ActiveRecord::Migration[5.2]
  def change
    create_table :invoices do |t|
      t.date :date
      t.boolean :is_paid
      t.references :customer, foreign_key: true
    end
  end
end
