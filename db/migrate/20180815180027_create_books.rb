class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :title
      t.string :author
      t.decimal :price, precision: 5, scale: 2
      t.references :publisher, foreign_key: true
    end
  end
end
