Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get    'countries',     to: 'countries#index'
  post   'countries',     to: 'countries#create'
  get    'countries/:id', to: 'countries#show'
  patch  'countries/:id', to: 'countries#update'
  put    'countries/:id', to: 'countries#update'
  delete 'countries/:id', to: 'countries#destroy'

  resources :publishers, shallow: true do
    resources :books do
      member do
        get :assets, to: 'book_assets#index'
      end
    end
  end


  resources :invoices, shallow: true do
    collection do
      get :search, to: 'invoice_searches#index'
    end
  end
end
